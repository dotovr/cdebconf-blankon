# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Traditional Chinese messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdebconf@packages.debian.org\n"
"POT-Creation-Date: 2009-07-02 20:15+0000\n"
"PO-Revision-Date: 2008-08-09 00:35+0800\n"
"Last-Translator: Tetralet <tetralet@gmail.com>\n"
"Language-Team: Debian-user in Chinese [Big5] <debian-chinese-big5@lists."
"debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "critical"
msgstr "關鍵"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "high"
msgstr "高"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "medium"
msgstr "中"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "low"
msgstr "低"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid "Ignore questions with a priority less than:"
msgstr "忽略問題，如果它的優先等級低於："

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"Packages that use debconf for configuration prioritize the questions they "
"might ask you. Only questions with a certain priority or higher are actually "
"shown to you; all less important questions are skipped."
msgstr ""
"如果套件是使用 debconf 來進行設定時，指定在設定過程中將被提問的問題的優先等"
"級。只有等於或高於這個優先等級的問題才會顯示並要求您回答﹔其它不太重要的問題"
"將會被略過。"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"You can select the lowest priority of question you want to see:\n"
" - 'critical' is for items that will probably break the system\n"
"    without user intervention.\n"
" - 'high' is for items that don't have reasonable defaults.\n"
" - 'medium' is for normal items that have reasonable defaults.\n"
" - 'low' is for trivial items that have defaults that will work in\n"
"   the vast majority of cases."
msgstr ""
"您可以選擇您想回答的問題的最低等級：\n"
" -【關鍵】 是指如果使用者沒有加以設定，將可能會使得系統無法正\n"
"           常運作的項目。\n"
" -【高】   是指預設值往往並不是很恰當的項目。\n"
" -【中】   是指預設值適用於多數情況的一般項目。\n"
" -【低】   是指在絕大多數情況下都可以放心使用其預設值的瑣碎項\n"
"           目。"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"For example, this question is of medium priority, and if your priority were "
"already 'high' or 'critical', you wouldn't see this question."
msgstr ""
"例如，這個問題的優先等級是【中】，而您所指定的優先等級卻是【高】或是【關"
"鍵】，那麼這個問題就會被直接略過了。"

#. Type: text
#. Description
#. :sl2:
#: ../cdebconf-priority.templates:1001
msgid "Change debconf priority"
msgstr "改變 debconf 的優先等級設定"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:1001 ../cdebconf-gtk-udeb.templates:1001
msgid "Continue"
msgstr "繼續"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:2001 ../cdebconf-gtk-udeb.templates:2001
msgid "Go Back"
msgstr "返回"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-newt-udeb.templates:3001 ../cdebconf-gtk-udeb.templates:3001
#: ../cdebconf-slang-udeb.templates:1001 ../cdebconf-text-udeb.templates:6001
msgid "Yes"
msgstr "是"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-newt-udeb.templates:4001 ../cdebconf-gtk-udeb.templates:4001
#: ../cdebconf-slang-udeb.templates:2001 ../cdebconf-text-udeb.templates:7001
msgid "No"
msgstr "否"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:5001
msgid "Cancel"
msgstr "取消"

#. Type: text
#. Description
#. Help line displayed at the bottom of the cdebconf newt interface.
#. Translators: must fit within 80 characters.
#. :sl1:
#: ../cdebconf-newt-udeb.templates:6001
msgid "<Tab> moves; <Space> selects; <Enter> activates buttons"
msgstr "<Tab> 移動；<Space> 選擇；<Enter> 按下按鈕"

#. Type: text
#. Description
#. Help line displayed at the bottom of the cdebconf newt interface.
#. Translators: must fit within 80 characters.
#. :sl1:
#: ../cdebconf-newt-udeb.templates:7001
msgid "<F1> for help; <Tab> moves; <Space> selects; <Enter> activates buttons"
msgstr "<F1> 求助；<Tab> 移動；<Space> 選擇；<Enter> 按下按鈕"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:5001
msgid "Help"
msgstr "求助"

#. Type: text
#. Description
#. TRANSLATORS: This should be "translated" to "RTL" or "LTR" depending of
#. default text direction of your language
#. LTR: Left to Right (Latin, Cyrillic, CJK, Indic...)
#. RTL: Right To Left (Arabic, Hebrew, Persian...)
#. DO NOT USE ANYTHING ELSE
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:6001
msgid "LTR"
msgstr "LTR"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. This button will allow users to virtually "shoot a picture"
#. of the screen
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:7001
msgid "Screenshot"
msgstr "螢幕快照"

#. Type: text
#. Description
#. Text that will appear in a dialog box mentioning which file
#. the screenshot has been saved to. "%s" is a file name here
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:8001
#, no-c-format
msgid "Screenshot saved as %s"
msgstr "將螢幕快照另存為 %s"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:1001
#, no-c-format
msgid "!! ERROR: %s"
msgstr "[!! 錯誤]：%s"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:2001
msgid "KEYSTROKES:"
msgstr "[鍵擊]："

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:3001
msgid "Display this help message"
msgstr "顯示這個求助資訊"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:4001
msgid "Go back to previous question"
msgstr "回到上一個問題"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:5001
msgid "Select an empty entry"
msgstr "選擇一個空項目"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:8001
#, no-c-format
msgid "Prompt: '%c' for help, default=%d> "
msgstr "提示：按 '%c' 求助，預設為：%d> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:9001
#, no-c-format
msgid "Prompt: '%c' for help> "
msgstr "提示：按 '%c' 求助> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:10001
#, no-c-format
msgid "Prompt: '%c' for help, default=%s> "
msgstr "提示：按 '%c' 求助，預設為：%s> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:11001
msgid "[Press enter to continue]"
msgstr "[按 enter 鍵繼續]"
